-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Jan 2020 pada 14.24
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `leate`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(30) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `tgl_lahir` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `level` varchar(10) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `gambar`, `username`, `password`, `tgl_lahir`, `email`, `alamat`, `level`, `status`) VALUES
(1, 'Abdul Mu\'ti Mf', 'flat,1000x1000as.jpg', 'ami', 'ami', '1996-07-20', 'abdulmu0809@bsi.ac.id', 'tasikmalaya', 'Admin', 'Aktif'),
(7, 'Akila sasqila', '', 'aki', 'aki', '2017-07-04', 'sas', 'sas', '', ''),
(11, 'ass', '', 'sas', 'asas', '2017-07-04', 'asas', 'sssss', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alumni`
--

CREATE TABLE `alumni` (
  `id_alumni` int(11) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `jenis_kelamin` enum('Perempuan','Laki-laki') NOT NULL,
  `id_jurusan` varchar(11) NOT NULL,
  `tgl_lahir` varchar(10) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `notlpn` varchar(30) NOT NULL,
  `Tahun_lulus` varchar(50) NOT NULL,
  `DN` varchar(2) NOT NULL,
  `MK` varchar(7) NOT NULL,
  `nis` varchar(8) NOT NULL,
  `lanjutan_pen` varchar(50) NOT NULL,
  `ket_pen` varchar(50) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `alamat_p` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alumni`
--

INSERT INTO `alumni` (`id_alumni`, `nama_lengkap`, `jenis_kelamin`, `id_jurusan`, `tgl_lahir`, `alamat`, `gambar`, `email`, `status`, `notlpn`, `Tahun_lulus`, `DN`, `MK`, `nis`, `lanjutan_pen`, `ket_pen`, `pekerjaan`, `alamat_p`) VALUES
(2, 'Aris Rismawan', 'Laki-laki', '2', '1987-10-01', 'Sukahaji Singaparna', 'alexander-ludwig-main.jpg', 'airrismawan@yahoo.com', 'Aktif', '081273112121', '2015', '02', '1082974', '121211', 'D3', 'aktif', '', ''),
(3, 'Gitalis Dwi Natarina', 'Perempuan', '2', '1990-09-09', 'Cilampung Hilir', 'url.jpg', 'gita@rocketmail.com', 'Aktif', '0898933333', '2013', '02', '1098233', '121212', '', '', '', ''),
(5, 'Sarah Sazkia', 'Perempuan', '2', '1990-09-01', 'Cikiray Singaparna', 'avatar3.png', 'sarah21@gmail.com', 'Aktif', '08123233222', '2013', '02', '1029833', '121214', '', '', '', ''),
(13, 'daniel', 'Laki-laki', '2', 'tasikmalay', '00', '', '00', '', '', '196', '', '', '121345', '', '', '', ''),
(15, 'kooper', 'Laki-laki', '3', '2017-07-03', 'tasikmalaya', '', 'as', '', '', 'asa', '', '', 'as', '', '', '', ''),
(17, 'denisa', 'Perempuan', '3', '2017-07-05', '', '', '', '', '', '', '', '', 'sasas', '', '', '', ''),
(19, 'dinar', 'Laki-laki', '4', '2017-07-05', 'gha', '', 'Saint.muti', '', '', '2010-', '', '', '121211', '', '', '', ''),
(20, 'tasika', 'Perempuan', '4', '2017-07-04', 'a', '', 'abdulmu0809@bsi.ac.id', '', '', '2009-', '', '', 'as', '', '', '', ''),
(21, 'daniel', 'Laki-laki', '2', '2017-07-05', 'tasikmalaya', '182560-Martin Garrix-2f69b9-large-1444270675.jpg', 'Saint.muti@gmail.com', '', '', '2011-', '', '', '12345', '', '', '', ''),
(22, 'Fitri alisa', 'Perempuan', '4', '1993-07-04', 'munich', '', 'muda', '', '', '2010-', '', '', '12345', '', '', '', ''),
(26, 'Jonas', 'Laki-laki', '2', '2017-09-03', 'tasikmalaya', 'alexander-ludwig-main.jpg', 'jonas_isac@gmail.com', '', '085121212', '2010-2011', '', '', '121210', '', '', '', ''),
(28, 'Kenny G', 'Perempuan', '4', '2017-01-01', 'tasikmalaya', 'avatar_handsome_guy-512.png', 'ati@gmail.com', '', '198777', '2010-2011', '', '', '2222', '', '', 'Operator', 'tasikmlaya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `jurusan` varchar(40) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `jurusan`, `keterangan`) VALUES
(2, 'Teknik Komputer Jaringan', 'mempelajari struktur jaringan'),
(3, 'Akuntansi', 'mengelola keuangan'),
(4, 'RPL', 'rekayasa komputer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lamaran`
--

CREATE TABLE `lamaran` (
  `id_lamaran` int(11) NOT NULL,
  `id_resume` int(11) NOT NULL,
  `tentang_saya` text NOT NULL,
  `id_loker` int(11) NOT NULL,
  `tanggal_lamaran` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lamaran`
--

INSERT INTO `lamaran` (`id_lamaran`, `id_resume`, `tentang_saya`, `id_loker`, `tanggal_lamaran`) VALUES
(1, 1, '', 1, '2016-10-9'),
(2, 1, 'Saya adalah lulusan terbaik saat D3', 4, '2016-10-10'),
(5, 1, 'saya mahir bahasa pemro', 5, '2016-08-24'),
(7, 0, 'dsad', 1, '23/05/2017'),
(8, 0, 'sadasd', 1, '23/05/2017'),
(9, 0, 'sadasd', 1, '23/05/2017'),
(11, 0, 'sadasd', 1, '23/05/2017'),
(12, 0, 'sdfsdf', 3, '23/05/2017'),
(13, 0, 'sdfsdf', 3, '23/05/2017'),
(17, 5, 'sad', 2, '23/05/2017'),
(46, 2, 'fogh', 1, '29/05/2017'),
(47, 3, 'sx', 4, '29/05/2017'),
(48, 4, 'kee', 1, '29/05/2017'),
(49, 4, 'keep', 5, '29/05/2017'),
(50, 3, 'nkk', 3, '30/05/2017'),
(53, 4, 'cause', 1, '03/06/2017'),
(54, 3, 'kim', 1, '03/06/2017'),
(55, 3, 'saya siap', 6, '08/06/2017'),
(56, 5, 'denail', 1, '20/07/2017'),
(57, 2, 'kakaka', 1, '20/07/2017'),
(58, 5, 'knl', 5, '20/07/2017'),
(59, 5, 'klknlkkn', 1, '20/07/2017'),
(60, 0, 'koo', 1, '20/07/2017'),
(61, 0, 'knknknkjn', 2, '20/07/2017'),
(62, 1, '1', 1, '1'),
(64, 6, 'tenantan', 2, '2017/07/22'),
(66, 6, 'saya siap bekerja dalam tekanan tinggi', 5, '2017/07/25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `loker`
--

CREATE TABLE `loker` (
  `id_loker` int(11) NOT NULL,
  `posisi` varchar(100) NOT NULL,
  `gaji` varchar(100) NOT NULL,
  `jenjang_pendidikan` enum('SMA','SMK','D3','S1','S2') NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `pengalaman` varchar(100) NOT NULL,
  `fasilitas` text NOT NULL,
  `tugas` text NOT NULL,
  `jaminan` text NOT NULL,
  `usia` varchar(3) NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan','Bebas') NOT NULL,
  `domisili` varchar(100) NOT NULL,
  `penempatan` varchar(100) NOT NULL,
  `gaya_berpakaian` varchar(100) NOT NULL,
  `waktu_bekerja` varchar(100) NOT NULL,
  `dibuka` varchar(10) NOT NULL,
  `ditutup` varchar(10) NOT NULL,
  `lainnya` text NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `jumlah` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `loker`
--

INSERT INTO `loker` (`id_loker`, `posisi`, `gaji`, `jenjang_pendidikan`, `jurusan`, `pengalaman`, `fasilitas`, `tugas`, `jaminan`, `usia`, `jenis_kelamin`, `domisili`, `penempatan`, `gaya_berpakaian`, `waktu_bekerja`, `dibuka`, `ditutup`, `lainnya`, `id_perusahaan`, `jumlah`) VALUES
(1, 'Data Entry Operator', 'Dirahasiakan', 'D3', 'Ekonomi, Teknik Informatika, Statistik', '0-1 Tahun Pengalaman Bekerja pada posisi yang sama / Fresh Graduated Dipersilahkan melamar', '-', 'Mengentri data Purchase Order (PO) dengan promosi dan aktivasi. \r\nMemonitoring Total promo. \r\nMengatur budget sesuai kebutuhan setiap bulan.', 'Tunjangan Hari Raya, Tunjangan Kesehatan, Tunjangan Anak.', '30', 'Perempuan', '-', 'Jakarta Barat ', 'Formal', 'Senin - Jum\'at (Pkl. 09.00-17.00)', '2016-07-25', '2016-08-20', '-', 8, '8'),
(2, 'Staf Sales-Support', 'Rp. 2.500.000- Rp. 3.000.000', 'D3', ' IT', ' Lulusan baru/pengalan kerja kurang dari 1 tahun', ' Disediakan Mes bagi yang domisili jauh', 'Melakukan administrasi berkaitan dengan marketing, Membuat surat jalan/jadwal pengiriman barang. Mencatat PO, DO retur barang.\r\n', 'Tunjangan Kesehatan', '25', 'Bebas', 'Tidak Ditentukan', 'Jakarta Selatan', 'Kasual (Contoh:Kaos)', 'Senin - Jumat, Pkl. 08.30-17.30', '2016-08-10', '2016-08-30', 'Menyukai pekerjaan administrasi', 9, ''),
(3, 'Staff Admin-Pembukuan', 'IDR Rp. 3.000.000,-', 'D3', 'Akuntansi', '2 tahun staff akunting/staff administrasi', '-', 'Mengatur Cash Flow pengeluaran keuangan perusahaan, Mencatat dan melaporkan pengeluaran, Bertanggung jawab terhadap dokumen-dokumen, Melakukan entry data dan filing data.', 'Tunjangan Kesehatan, Bantuan Pendidikan, Penglihatan, etc', '27', 'Perempuan', 'Tidak Ditentukan', 'Jl. Raya Bekasi KM 25, Cakung Jakarta Timur', 'Bisnis (Contoh:Kemeja)', 'Senin - Jumat, Pkl. 08.30-17.30', '2016-08-10', '2016-09-09', 'Mengerti Aplikasi Microsoft Word dan Excel', 2, ''),
(4, 'Staff HRD', 'IDR Rp. 4.000.000,-', 'S1', ' Psikologi', ' 2 tahun di bidang yang sama', ' Diberikan mes bagi pelamar yang berdomisili jauh', 'Menginterpretasikan alat psikotes dengan baik, menghitung dan merekap absensi, menjalin kerjasama yang baik antar karyawan, memahami ISO 9001, dll', 'Tunjangan Kesehatan', '28', 'Bebas', 'Tidak Ditentukan', 'Jl. Gebong Baru Utara Tomang, Jakarta Selatan', 'Bisnis (Contoh:Kemeja)', 'Senin - Jumat, Pkl. 08.30-16.30', '2016-08-10', '2016-10-01', 'IPK MInimal 3,00, Memiliki Kerapihan Filling Dokumen yang baik', 2, ''),
(5, 'Admin Finance', 'Dirahasiakan', 'D3', 'Finance\r\n', '0-1 tahun/Fresh Graduated dipersilahkan untuk melamar', '', 'mengurus masalah keuangan, membuat laporan optional, input data laporan keuangan ke sistem accurate', 'Tunjangan Kesehatan, Parkir, Tunjangan Hari Raya, dll-', '30', 'Bebas', 'Tidak Ditentukan', 'Jakarta Pusat', 'Bismis (Contoh:Kemeja)', 'Jam bekerja yang panjang', '2016-07-25', '2016-09-20', 'Menguasai Ms. Word', 7, ''),
(6, 'Admin', '900000', 'D3', 'Teknik Komputer Jaringan', '2 tahun', 'free ongkir', 'input data', 'Jamsostek', '30', 'Bebas', 'tasikmlaya', 'tasikmalaya', 'Non-Formal', '09.00-12.00', '19 mei', '20 juni', 'bonus', 21, ''),
(7, 'Admin', '3000.000', 'D3', 'Komputer', 'tidak diutaman', 'uang makan', 'input data', 'uang lembur', '31', 'Perempuan', 'tasikmlaya', 'tasikmalaya', '', 'jam 1-3 sore', '2017-07-24', '2017-07-27', 'tidak ad', 8, '19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemberitahuan`
--

CREATE TABLE `pemberitahuan` (
  `id_pemberitahuan` int(11) NOT NULL,
  `id_lamaran` int(11) NOT NULL,
  `jenis_pemberitahuan` enum('Wawancara','Tunggu','Ditolak') NOT NULL,
  `tanggal_wawancara` varchar(10) NOT NULL,
  `waktu` varchar(5) NOT NULL,
  `tempat` text NOT NULL,
  `tanggal_dikirim` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemberitahuan`
--

INSERT INTO `pemberitahuan` (`id_pemberitahuan`, `id_lamaran`, `jenis_pemberitahuan`, `tanggal_wawancara`, `waktu`, `tempat`, `tanggal_dikirim`) VALUES
(1, 3, 'Tunggu', 'aa', '-asa', 'tasik', '2016-08-17'),
(3, 3, 'Wawancara', '2016-10-09', '12:12', '', '2016-08-18'),
(4, 54, 'Wawancara', '19', '19', 'tasikmalaya', '9'),
(5, 53, 'Tunggu', '5', '5', '5', '5'),
(6, 53, 'Wawancara', 'dasd', 'asdas', 'dasd', '10/06/2017'),
(7, 56, 'Tunggu', '19994', 'tasik', 'asas', '20/07/2017'),
(8, 56, 'Tunggu', 'tasik', 'asas', 'asas', '20/07/2017'),
(10, 57, 'Wawancara', 'asdasd', 'dasd', 'sdasd', '20/07/2017'),
(11, 56, 'Wawancara', '121212', 'sdsds', 'dsdsd', '20/07/2017'),
(13, 62, 'Tunggu', 'asdasd', 'sdasd', 'asdasd', '20/07/2017'),
(14, 57, 'Wawancara', '00', '', '21212', '12121212'),
(15, 57, 'Tunggu', 'zczxc', 'zxcxz', 'czxc', '23/07/2017'),
(18, 56, 'Wawancara', '09-081995', 'jam 9', 'tasikmalaya', '25/07/2017'),
(19, 46, 'Tunggu', '2017-06-27', '20:57', '-', '2017-07-04'),
(20, 46, 'Wawancara', '2017-07-04', '11:58', 'tasikmalaya', '2017-07-05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perusahaan`
--

CREATE TABLE `perusahaan` (
  `id_perusahaan` int(11) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `nama_pencari` varchar(30) NOT NULL,
  `email_perusahaan` varchar(50) NOT NULL,
  `telp_perusahaan` varchar(20) NOT NULL,
  `keterangan_perusahaan` text NOT NULL,
  `password_perusahaan` varchar(10) NOT NULL,
  `alamat_perusahaan` text NOT NULL,
  `surat` enum('SUDAH','BELUM') NOT NULL,
  `setuju` enum('TUNGGU','TIDAK','YA') NOT NULL,
  `nama_surat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `perusahaan`
--

INSERT INTO `perusahaan` (`id_perusahaan`, `nama_perusahaan`, `nama_pencari`, `email_perusahaan`, `telp_perusahaan`, `keterangan_perusahaan`, `password_perusahaan`, `alamat_perusahaan`, `surat`, `setuju`, `nama_surat`) VALUES
(2, 'NAC 123 Learning Center', 'eka Triak', 'nac123@nac.com', '(021) 3257732', 'Perusahaan yang bergerak di bidang Pendidikan Bimbingan Belajar Berbasis Internasional', 'kuranggizi', 'Jl. Gebong Baru Barat Tomang, Jakarta Selatan', 'SUDAH', 'YA', ''),
(4, 'PT Golden Prima Group', ' Susi Susilawati', 'goldenprima@rocketmail.com', '(022) 7825 374', 'Perusahaan ini bergerak di bidang pabrikasi gula pasir dan pemanfaatan tebu', 'gol12345', 'Jl. Gebong Baru Timur Tomang, Jakarta Selatan', 'BELUM', 'TIDAK', ''),
(5, 'Wings Group', 'Wanda Feronika', 'wings@wings.com', '(021)3257733', 'Perusahaan BEsar', 'win12345', 'Jakarta Barat', 'BELUM', 'TIDAK', ''),
(6, 'PT Yayan Emas', 'Susi Susilawati', 'yayanemas@ymail.com', '089772772771', 'Perusahaan toko emas', 'yay12345', 'Jakarta Selatan', 'BELUM', 'TIDAK', ''),
(7, 'PT Rumasa Nalangsa', 'Siska Dwi Utami', 'rumasanalangsa@yahoo.com', '(021)3257833', 'Perusahaan Majalah', 'rum12345', 'Jl. Rumah Sakit Tasikmalaya', 'SUDAH', 'YA', ''),
(8, 'PT L\'Oreal Indonesia', 'Wilian', 'loreal@gmail.com', '(021)298 6667', 'Pada Tahun 1976, untuk pertama kalinya L\'Oreal hadir di Indonesia lewat Lancome, salah satu merk kosmetik Luxury.', 'lor4561588', 'Jl. Raya Tomang No 98 Jakarta Raya-Jakarta Barat', 'SUDAH', 'YA', '0001.jpg'),
(9, 'PT ADR Argo', 'Aisyah Fahrun Nisa', 'adr_group@adr.com', '(021) 6615555', 'Perusahaan yang bergerak dibidang argo', 'adr12345', 'Pluit, Jakarta Raya, Indonesia', 'SUDAH', 'YA', ''),
(10, 'PT Sinar Gemilang', 'Sony Gumilar', 'sinargemilang@yahoo.com', '(021) 6615555', 'Perusahaan yang bergerak dibidang pertanian\r\n', 'sin12345', 'Jl. Gatot Subroto Jakarta Pusat', 'BELUM', 'TIDAK', ''),
(13, 'Contoh lain', 'Larissa Cou', 'contoh@lain.com', '123', 'Sa\r\n', 'con12345', 'Contoh lain', 'SUDAH', 'TUNGGU', 'doc.pdf'),
(14, 'contoh123', 'contoh123', 'contoh123@gmail.com', '021890', 'jjjjjjj', 'con12345', 'jjjjjj', 'SUDAH', 'YA', 'silabus spreasheet iso 2011.doc'),
(17, 'Calorina Vaper', 'heile stanfield', 'calorina@gmail.com', '0897776777', 'perusahaan bergerak dibidang percabean', 'calorina', 'jl.kebenaran no 1', 'SUDAH', 'TIDAK', '4-ijazah-d3-jualijazahasli-wordpress-com.jpg'),
(21, 'pt/p', 'aji', 'aji@bsi.ac.id', '09999', 'perusahaan ', '12345', 'tasikmalaya', 'SUDAH', 'TUNGGU', 'ChoiceSchoolClipart-300x300.jpg'),
(23, 'better', 'akil', 'sa', 'asa', '', 'asa', '', 'SUDAH', 'TUNGGU', ''),
(25, 'Nippa', 'deny', 'sas@gsham.om', '089', '0899', '00000', '', 'SUDAH', 'TUNGGU', ''),
(27, 'koa', 'as', 'say@gmail.com', '97777', 'tasik', '12345', '', 'SUDAH', 'TUNGGU', 'download (1).png'),
(28, 'keep', 'kepep', 'kepep', 'kepep', '', 'kepep', '', 'SUDAH', 'TUNGGU', '2013-08-25-346294_20130825084350.JPG'),
(29, 'PT.angkasa raya', 'ahmad', 'angkasa@gmail.com', '08923823', 'perusahaan dibidan pertanian', '123456', '', 'SUDAH', 'TUNGGU', ''),
(30, 'PT.Smanggin', 'ahmad', 'smanggin@gmail.com', '08923823', '0892930231', '12345', '', 'SUDAH', 'TUNGGU', 'Contoh-SUrat.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `resume`
--

CREATE TABLE `resume` (
  `id_resume` int(11) NOT NULL,
  `id_alumni` int(11) NOT NULL,
  `cv` varchar(100) NOT NULL,
  `ijazah` varchar(100) NOT NULL,
  `lain2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `resume`
--

INSERT INTO `resume` (`id_resume`, `id_alumni`, `cv`, `ijazah`, `lain2`) VALUES
(1, 6, 'kartu_wisuda.png', 'avatar_handsome_guy-512', 'barkode.png'),
(2, 2, 'alexander-ludwig-main.jpg', 'dd966404gw1eyd3xdteqgj21jk16lq6z.jpg', 'ijazah-d-iv-56c540144373e.jpg'),
(3, 1, 'diploma-1-interstudi (1).jpg', 'contoh-cv-bahasa-inggris-stephanie-1-638.jpg', 'sim-riyanto.jpg'),
(4, 3, 'Untitled.png', 'avatar_handsome_guy-512.png', '155108_28a9c574-0841-11e5-9672-5cdf49bc7260.jpg'),
(5, 26, '19059420_873352236148532_2740782805285656039_n.jpg', '19059420_873352236148532_2740782805285656039_n.jpg', '18620352_1535781903210908_9101590539889675559_n.jpg'),
(6, 28, 'photo.jpg', 'download.jpg', 'erdplus-diagram (2).png'),
(15, 2, '728_2a_1.png', 'bingung.gif', '728_2copy.png'),
(18, 29, 'sample.png', '304753627_2_644x461_modem-gsm-3g-huawei-kenceng-stabil-upload-foto.jpg', 'Untitle.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sample`
--

CREATE TABLE `sample` (
  `id_sample` int(3) NOT NULL,
  `nama_sample` varchar(13) NOT NULL,
  `sample` varchar(100) NOT NULL,
  `ket` varchar(44) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id_alumni`);

--
-- Indeks untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indeks untuk tabel `lamaran`
--
ALTER TABLE `lamaran`
  ADD PRIMARY KEY (`id_lamaran`);

--
-- Indeks untuk tabel `loker`
--
ALTER TABLE `loker`
  ADD PRIMARY KEY (`id_loker`);

--
-- Indeks untuk tabel `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  ADD PRIMARY KEY (`id_pemberitahuan`);

--
-- Indeks untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indeks untuk tabel `resume`
--
ALTER TABLE `resume`
  ADD PRIMARY KEY (`id_resume`);

--
-- Indeks untuk tabel `sample`
--
ALTER TABLE `sample`
  ADD PRIMARY KEY (`id_sample`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id_alumni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `lamaran`
--
ALTER TABLE `lamaran`
  MODIFY `id_lamaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT untuk tabel `loker`
--
ALTER TABLE `loker`
  MODIFY `id_loker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pemberitahuan`
--
ALTER TABLE `pemberitahuan`
  MODIFY `id_pemberitahuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `resume`
--
ALTER TABLE `resume`
  MODIFY `id_resume` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `sample`
--
ALTER TABLE `sample`
  MODIFY `id_sample` int(3) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
