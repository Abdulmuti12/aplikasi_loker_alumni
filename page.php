<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Maxim - Modern One Page Bootstrap Template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- css -->
<link href="alumni/css/bootstrap-responsive.css" rel="stylesheet">
<link href="alumni/css/style.css" rel="stylesheet">
<!-- skin color -->
<link href="alumni/color/default.css" rel="stylesheet">
<!--[if lt IE 7]>
            <link href="css/font-awesome-ie7.css" type="text/css" rel="stylesheet">  
        <![endif]-->
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<!-- Favicon -->
<link rel="shortcut icon" href="alumni/img/favicon.ico">
</head>
<body>
<!-- navbar -->
<div class="navbar-wrapper">
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<!-- Responsive navbar -->
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</a>
				<h2 class="brand"><a href="index.html">BSI</a></h2>
				<!-- navigation -->
				<nav class="pull-right nav-collapse collapse">
				<ul id="menu-main" class="nav">
					<li><a title="team" href="#about">About</a></li>
					<li><a title="services" href="index.php#services">Jurusan</a></li>
					<li><a title="works" href="index.php#works">Lowongan</a></li>
					<li><a title="blog" href="index.php#blog">Info Job Fair</a></li>
					<li><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Login
  <span class="caret"></span></button></a>
  
  <ul class="dropdown-menu">
    <li><a href="alumni/alumni_login.php">Alumni</a></li>
    <li><a href="perusahaan/login.php">Perusahaan</a></li>
  </ul>

  </li>
  
				</ul>
				</nav>
			</div>
		</div>
	</div>
</div>


<!-- spacer section -->
<!-- end spacer section -->
<!-- section: team -->
<section id="maincontent" class="inner">
 <!-- end container -->
 <h2 class="text-center brand" align="center"><b>Daftar Lowongan Kerja</b></h2>
<?php include "lokert.php"; ?>
</section>

<footer>
<div class="container">
	<div class="row">
		<div class="span6 offset3">
			<ul class="social-networks">
				<li><a href="#"><i class="icon-circled icon-bgdark icon-instagram icon-2x"></i></a></li>
				<li><a href="#"><i class="icon-circled icon-bgdark icon-twitter icon-2x"></i></a></li>
				<li><a href="#"><i class="icon-circled icon-bgdark icon-dribbble icon-2x"></i></a></li>
				<li><a href="#"><i class="icon-circled icon-bgdark icon-pinterest icon-2x"></i></a></li>
			</ul>
			<p class="copyright">
				&copy; 2013 Maxim Inc corporate. All rights reserved.
			</p>
		</div>
	</div>
</div>
<!-- ./container -->
</footer>
<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
<!-- jQuery -->
<script src="alumni/js/jquery.js"></script>
<!-- nav -->
<script src="alumni/js/jquery.scrollTo.js"></script>
<script src="alumni/js/jquery.nav.js"></script>
<!-- localScroll -->
<script src="alumni/js/jquery.localscroll-1.2.7-min.js"></script>
<!-- bootstrap -->
<script src="alumni/js/bootstrap.js"></script>
<!-- prettyPhoto -->
<script src="alumni/js/jquery.prettyPhoto.js"></script>
<!-- Works scripts -->
<script src="alumni/js/isotope.js"></script>
<!-- flexslider -->
<script src="js/jquery.flexslider.js"></script>
<!-- inview -->
<script src="alumni/js/inview.js"></script>
<!-- animation -->
<script src="alumni/js/animate.js"></script>
<!-- twitter -->
<script src="alumni/js/jquery.tweet.js"></script>
<!-- contact form -->
<script src="alumni/js/validate.js"></script>
<!-- custom functions -->
<script src="alumni/js/custom.js"></script>
</body>
</html>