<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO loker (id_loker, posisi, gaji, jenjang_pendidikan, jurusan, pengalaman, fasilitas, tugas, jaminan, usia, jenis_kelamin, domisili, penempatan, gaya_berpakaian, waktu_bekerja, dibuka, ditutup, lainnya, id_perusahaan, jumlah) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_loker'], "int"),
                       GetSQLValueString($_POST['posisi'], "text"),
                       GetSQLValueString($_POST['gaji'], "text"),
                       GetSQLValueString($_POST['jenjang_pendidikan'], "text"),
                       GetSQLValueString($_POST['jurusan'], "text"),
                       GetSQLValueString($_POST['pengalaman'], "text"),
                       GetSQLValueString($_POST['fasilitas'], "text"),
                       GetSQLValueString($_POST['tugas'], "text"),
                       GetSQLValueString($_POST['jaminan'], "text"),
                       GetSQLValueString($_POST['usia'], "text"),
                       GetSQLValueString($_POST['jenis_kelamin'], "text"),
                       GetSQLValueString($_POST['domisili'], "text"),
                       GetSQLValueString($_POST['penempatan'], "text"),
                       GetSQLValueString($_POST['gaya_berpakaian'], "text"),
                       GetSQLValueString($_POST['waktu_bekerja'], "text"),
                       GetSQLValueString($_POST['dibuka'], "text"),
                       GetSQLValueString($_POST['ditutup'], "text"),
                       GetSQLValueString($_POST['lainnya'], "text"),
                       GetSQLValueString($_POST['id_perusahaan'], "int"),
                       GetSQLValueString($_POST['jumlah'], "text"));

   $Result1 = mysql_query($insertSQL) or die(mysql_error());


  
  if (isset($_SERVER['QUERY_STRING'])) {
echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php?aksi=5">';



      }

 }

?>

    <body>
    <div class="container">      
    
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- konten modal-->
            <div class="modal-content">
                <!-- heading modal -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Loker</h4>
                </div>
                <!-- body modal -->
                <div class="modal-body">
                
                  <form method="post" name="form1" action="<?php echo $editFormAction; ?>">
                    <table class="table">
                    <center>
                        <input type="hidden" name="id_loker"  size="32">
                     
                       <tr>
                        <td><input type="text" name="posisi" value="" placeholder="posisi" class="form-control"></td>
                        <td><input type="text" name="gaji" value="" placeholder="Gaji" class="form-control"></td>
                      </tr>
                      <tr valign="baseline">
                        <td> <select name="jenjang_pendidikan" class="form-control">
                        <option value="SMK">SMK</option>
                        <option value="D3">D3</option>
                        <option value="S1">S1</option>
                        </select>
                        </td>
                        
                        <td><input type="text" name="jurusan" value="" placeholder="jurusan" class="form-control"></td>
                      </tr>
                      
                        <td><textarea name="pengalaman" cols="40" rows="4" class="form-control" placeholder="Pengalaman"></textarea></td>
                     
                        <td><textarea name="fasilitas" class="form-control" cols="40" rows="4" placeholder="fasilitas"></textarea></td>
                      </tr>
                     
                        <td><textarea class="form-control" cols="40" rows="4"name="tugas" placeholder="tugas"></textarea></td>
                      
                        <td><textarea class="form-control" cols="40" rows="4" name="jaminan" placeholder="Jaminan"></textarea></td>
                      </tr>
                      <tr>
                       
                        <td><input type="number" name="usia" class="form-control" placeholder="usia"></td>
                      <td><select name="jenis_kelamin" class="form-control">
                      <option value="laki-laki">Laki-laki</option>
                      <option value="perempuan">Perempuan</option>
                      <option value="bebas">Bebas</option>
                      </select>
                      </td>
                      
                        <td><input type="text" name="domisili" value="" size="32" placeholder="Domisili" class="form-control"></td>
                      </tr>
                      <tr>
                        
                        <td><input type="text" name="penempatan" placeholder="Penempatan" class="form-control"></td>
                     
                        <td><select name="gaya_berpakaian" class="form-control">
                        <option value="Non Formal">Non Formal</option>
                        <option value="bebas">Formal</option>
                        </select>
                        
                        </td>
                    
                        <td><input type="text" name="waktu_bekerja" value="" size="32" class="form-control" placeholder="Waktu Bekerja"></td>
                      </tr>
                      <tr>
                    <td><input type="date" name="dibuka" value="" class="form-control" placeholder="dibuka"></td>
                       <td><input type="date" name="ditutup" value="" class="form-control" placeholder="ditutup"></td>
                       <td><input type="text" name="jumlah" placeholder="jumlah Tenaga kerja" class="form-control"></td>
                       </tr>
                       <tr>
                        <td><textarea class="form-control" cols="80" rows="8" name="lainnya" placeholder="Lainya" class="form-control"></textarea></td>
                     
                        
                      </tr>
                      <input type="hidden" name="id_perusahaan" value="<?php echo $row_Recordset1['id_perusahaan']; ?>">
                     
                  </center>
                    </table>
                    <input type="submit" value="Tambah Loker" class="btn btn-primary">
                    <input type="hidden" name="MM_insert" value="form1">
                  </form>
                  
                  <p>&nbsp;</p>
                </div>
                <!-- footer modal -->
                <div class="modal-footer">
                    
              </div>
            </div>
        </div>
    </div>
   </div>        
    </body>
</html>