
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE perusahaan SET nama_perusahaan=%s, nama_pencari=%s, email_perusahaan=%s, telp_perusahaan=%s, keterangan_perusahaan=%s, alamat_perusahaan=%s WHERE id_perusahaan=%s",
                       GetSQLValueString($_POST['nama_perusahaan'], "text"),
                       GetSQLValueString($_POST['nama_pencari'], "text"),
                       GetSQLValueString($_POST['email_perusahaan'], "text"),
                       GetSQLValueString($_POST['telp_perusahaan'], "text"),
                       GetSQLValueString($_POST['keterangan_perusahaan'], "text"),
                       GetSQLValueString($_POST['alamat_perusahaan'], "text"),
                       GetSQLValueString($_POST['id_perusahaan'], "int"));

 
  $Result1 = mysql_query($updateSQL) or die(mysql_error());

  if (isset($_SERVER['QUERY_STRING'])) {
echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php?aksi=1">';



      }

 }

$colname_Recordsea = "-1";
if (isset($_GET['id_perusahaan'])) {
  $colname_Recordsea = $_GET['id_perusahaan'];
}

$query_Recordsea = sprintf("SELECT id_perusahaan, nama_perusahaan, nama_pencari, email_perusahaan, telp_perusahaan, keterangan_perusahaan, alamat_perusahaan FROM perusahaan WHERE id_perusahaan = %s", GetSQLValueString($colname_Recordsea, "int"));
$Recordsea = mysql_query($query_Recordsea) or die(mysql_error());
$row_Recordsea = mysql_fetch_assoc($Recordsea);
$totalRows_Recordsea = mysql_num_rows($Recordsea);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
  <br />
<br />

  <table class="table">
   
    <tr >
      <td nowrap="nowrap" align="right">Nama Perusahaan:</td>
      <td><input type="text" name="nama_perusahaan"  class="form-control" value="<?php echo htmlentities($row_Recordsea['nama_perusahaan'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Perwakilan:</td>
      <td><input type="text" name="nama_pencari" class="form-control" value="<?php echo htmlentities($row_Recordsea['nama_pencari'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email Perusahaan:</td>
      <td><input type="text" name="email_perusahaan" class="form-control" value="<?php echo htmlentities($row_Recordsea['email_perusahaan'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Telephone perusahaan:</td>
      <td><input type="text" name="telp_perusahaan" class="form-control" value="<?php echo htmlentities($row_Recordsea['telp_perusahaan'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Keterangan Perusahaan:</td>
      <td><textarea cols="30" rows="3" name="keterangan_perusahaan" class="form-control"><?php echo htmlentities($row_Recordsea['keterangan_perusahaan'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Alamat perusahaan:</td>
      <td><textarea cols="30" rows="3" name="alamat_perusahaan" class="form-control"><?php echo htmlentities($row_Recordsea['alamat_perusahaan'], ENT_COMPAT, 'utf-8'); ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><button type="submit" class="btn btn-default"><i class=" fa fa-refresh ">Update</i></button>
      <button type="submit" class="btn btn-danger"><i class=" fa fa-exclamation-triangle ">Cancel</i></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_perusahaan" value="<?php echo $row_Recordsea['id_perusahaan']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($Recordsea);
?>
