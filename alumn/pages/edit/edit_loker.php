
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE loker SET posisi=%s, gaji=%s, jenjang_pendidikan=%s, jurusan=%s, pengalaman=%s, fasilitas=%s, tugas=%s, jaminan=%s, usia=%s, jenis_kelamin=%s, domisili=%s, penempatan=%s, gaya_berpakaian=%s, waktu_bekerja=%s, dibuka=%s, ditutup=%s, lainnya=%s, jumlah=%s WHERE id_loker=%s",
                       GetSQLValueString($_POST['posisi'], "text"),
                       GetSQLValueString($_POST['gaji'], "text"),
                       GetSQLValueString($_POST['jenjang_pendidikan'], "text"),
                       GetSQLValueString($_POST['jurusan'], "text"),
                       GetSQLValueString($_POST['pengalaman'], "text"),
                       GetSQLValueString($_POST['fasilitas'], "text"),
                       GetSQLValueString($_POST['tugas'], "text"),
                       GetSQLValueString($_POST['jaminan'], "text"),
                       GetSQLValueString($_POST['usia'], "text"),
                       GetSQLValueString($_POST['jenis_kelamin'], "text"),
                       GetSQLValueString($_POST['domisili'], "text"),
                       GetSQLValueString($_POST['penempatan'], "text"),
                       GetSQLValueString($_POST['gaya_berpakaian'], "text"),
                       GetSQLValueString($_POST['waktu_bekerja'], "text"),
                       GetSQLValueString($_POST['dibuka'], "text"),
                       GetSQLValueString($_POST['ditutup'], "text"),
                       GetSQLValueString($_POST['lainnya'], "text"),
                       GetSQLValueString($_POST['jumlah'], "text"),
                       GetSQLValueString($_POST['id_loker'], "int"));

 
  $Result1 = mysql_query($updateSQL) or die(mysql_error());
if (isset($_SERVER['QUERY_STRING'])) {
echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php?aksi=5">';



      }

 }

$colname_keep = "-1";
if (isset($_GET['id_loker'])) {
  $colname_keep = $_GET['id_loker'];
}

$query_keep = sprintf("SELECT id_loker, posisi, gaji, jenjang_pendidikan, jurusan, pengalaman, fasilitas, tugas, jaminan, usia, jenis_kelamin, domisili, penempatan, gaya_berpakaian, waktu_bekerja, dibuka, ditutup, lainnya, jumlah FROM loker WHERE id_loker = %s", GetSQLValueString($colname_keep, "int"));
$keep = mysql_query($query_keep) or die(mysql_error());
$row_keep = mysql_fetch_assoc($keep);
$totalRows_keep = mysql_num_rows($keep);
?>
<body>
<br>

  <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
<i class="fa fa-align-justify">
                        </i> Data Loker
</div>
                        <div class="panel panel-body">


<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
<input type="hidden" name="id_loker" value="<?php echo $row_keep['id_loker'];?>">
<div class="col-md-6 col-sm-6">
<div class="panel panel-default">
<div class="panel panel-body">

<table class="table">
<tr>
<th>Posisi</th><td><input type="text" class="form-control text-center" name="posisi" value="<?php echo htmlentities($row_keep['posisi'], ENT_COMPAT, ''); ?>" size="32"></td>
</tr>
<tr>
<th>Gaji</th><td><input type="text" class="form-control text-center" name="gaji" value="<?php echo htmlentities($row_keep['gaji'], ENT_COMPAT, ''); ?>" ></td>
</tr>
<tr>
<th>Pendidikan</th><td><input type="text" class="form-control text-center" name="jenjang_pendidikan" value="<?php echo htmlentities($row_keep['jenjang_pendidikan'], ENT_COMPAT, ''); ?>" ></td>
</tr>
<tr>
<th>Jurusan</th><td><input type="text" class="form-control text-center" name="jurusan" value="<?php echo htmlentities($row_keep['jurusan'], ENT_COMPAT, ''); ?>" ></td>
</tr>
<tr>
<th>Penempatan</th><td><input type="text" class="form-control text-center" name="penempatan" value="<?php echo htmlentities($row_keep['penempatan'], ENT_COMPAT, ''); ?>" size="32"></td>
</tr>
<tr>
<th>Usia</th><td><input type="text" class="form-control text-center" name="usia" value="<?php echo htmlentities($row_keep['usia'], ENT_COMPAT, ''); ?>" size="32"></td>
</tr>

<tr>
<th>Domisili</th><td><input type="text" class="form-control text-center" name="domisili" value="<?php echo htmlentities($row_keep['domisili'], ENT_COMPAT, ''); ?>" size="32"></td>
</tr>

<tr>
<th>Pakaian</th><td><select name="gaya_berpakaian" class="form-control text-center">
        <option value="" >Formal</option>
        <option value="" >Non-formal</option>
      </select></td>
</tr>

<tr>
 <th>Jumlah</th><td><input type="text" class="form-control text-center" name="jumlah" value="<?php echo htmlentities($row_keep['jumlah'], ENT_COMPAT, ''); ?>" size="32"></td>

<tr>
    <th>Waktu Bekerja</th><td><input type="text" class="form-control text-center" name="waktu_bekerja" value="<?php echo htmlentities($row_keep['waktu_bekerja'], ENT_COMPAT, ''); ?>" size="32"></td>
</tr>
<tr>
<th>Jenis Kelamin</th>
<td><input type="radio" name="jenis_kelamin" value="Laki-laki" <?php if (!(strcmp(htmlentities($row_keep['jenis_kelamin'], ENT_COMPAT, ''),"Laki-laki"))) {echo "checked=\"checked\"";} ?>> laki-laki
<input type="radio" name="jenis_kelamin" value="Perempuan" <?php if (!(strcmp(htmlentities($row_keep['jenis_kelamin'], ENT_COMPAT, ''),"Perempuan"))) {echo "checked=\"checked\"";} ?>>
perempuan
<input type="radio" name="jenis_kelamin" value="Bebas" <?php if (!(strcmp(htmlentities($row_keep['jenis_kelamin'], ENT_COMPAT, ''),"Bebas"))) {echo "checked=\"checked\"";} ?>>
            bebas</td>
</tr>
</table>

</div>
</div>
</div>

<div class="col-md-6 col-sm-6">
<div class="panel panel-default">
<div class="panel panel-body">
<table class="table">
<tr>
<th>Fasilitas</th><td><textarea name="fasilitas"  class="form-control text-center" cols="20" rows="3"><?php echo htmlentities($row_keep['fasilitas'], ENT_COMPAT, ''); ?></textarea></td>
</tr>
<tr>
<th>Tugas</th>
<td><textarea name="tugas"  class="form-control text-center" cols="20" rows="3"><?php echo htmlentities($row_keep['tugas'], ENT_COMPAT, ''); ?></textarea></td>
</tr>
<tr>
<th>Jaminan</th><td><textarea name="jaminan"  class="form-control text-center" cols="20" rows="3"><?php echo htmlentities($row_keep['jaminan'], ENT_COMPAT, ''); ?></textarea></td>
</tr>
<tr>
<th>Pengalaman</th><td><textarea name="pengalaman"  class="form-control text-center" cols="20" rows="3"><?php echo htmlentities($row_keep['pengalaman'], ENT_COMPAT, ''); ?></textarea></td>
</tr>
<tr>
<th>Lainya</th><td><textarea name="lainnya"  class="form-control text-center" cols="20" rows="3"><?php echo htmlentities($row_keep['lainnya'], ENT_COMPAT, ''); ?></textarea></td>
</tr>
<tr>
<th>Dibuka</th><td><input type="date" class="form-control text-center" value="<?php echo htmlentities($row_keep['ditutup'], ENT_COMPAT, ''); ?>" name="dibuka"></td>
</tr>
<tr>
<th>Ditutup</th><td><input type="date" class="form-control text-center" name="ditutup" value="<?php echo htmlentities($row_keep['ditutup'], ENT_COMPAT, ''); ?>"></td>
</tr>
</table>

</div>
</div>
</div>


</div>
<div class="panel-footer">

<button type="submit" class="btn btn-default"><i class=" fa fa-refresh ">Update</i></button>
<button type="submit" class="btn btn-danger"><i class=" fa fa-exclamation-triangle ">Cancel</i></button>
<input type="hidden" name="MM_update" value="form1">
</form>

</div>
</div>
</div>


</body>
<?php
mysql_free_result($keep);
?>
