<?php require_once('../../Connections/connection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO perusahaan (id_perusahaan, nama_perusahaan, nama_pencari, email_perusahaan, telp_perusahaan, keterangan_perusahaan, password_perusahaan) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_perusahaan'], "int"),
                       GetSQLValueString($_POST['nama_perusahaan'], "text"),
                       GetSQLValueString($_POST['nama_pencari'], "text"),
                       GetSQLValueString($_POST['email_perusahaan'], "text"),
                       GetSQLValueString($_POST['telp_perusahaan'], "text"),
                       GetSQLValueString($_POST['keterangan_perusahaan'], "text"),
                       GetSQLValueString($_POST['password_perusahaan'], "text"));

  mysql_select_db($database_connection, $connection);
  $Result1 = mysql_query($insertSQL, $connection) or die(mysql_error());

  $insertGoTo = "login.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading text-center">
                        Daftar Perusahaan
                    </div>
                    <div class="panel-body">




<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
   <fieldset>
                                <div class="form-group input-group">    
      <input type="hidden" name="id_perusahaan" value="" class="form-control">
      </div>
    <div class="form-group input-group"> 
       <span class="input-group-addon"><i class="fa fa-list-alt"></i></span><input type="text" name="nama_perusahaan" class="form-control" placeholder="Nama Perusahaan">
      </div>
     <div class="form-group input-group"> 
      
     <span class="input-group-addon"><i class="fa fa-user"></i></span> <input type="text" name="nama_pencari" class="form-control" placeholder="Nama Perwakilan">
  </div>
     <div class="form-group input-group"> 
      
     <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
     <input type="email" name="email_perusahaan" placeholder="Email Perusahaan" class="form-control">
     </div>
    <div class="form-group input-group"> 
    <span class="input-group-addon"><i class="fa fa-tablet"></i></span>
      <input type="tel" name="telp_perusahaan" value="" class="form-control" placeholder="Nomor Tlpn">
    </div>
    <div class="form-group input-group"> 
    <span class="input-group-addon"><i class="fa fa-file-o"></i></span>
    <input type="text" name="keterangan_perusahaan" value="" placeholder="Keterangan Perusahaan" class="form-control">
    </div>
    <div class="form-group input-group"> 
    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
      <input type="password" name="password_perusahaan" value="" placeholder="Password" class="form-control">
    </div>
     <button type="submit" class="btn btn-lg btn-primary btn-block">Daftar</button>
    
  </fieldset>
    <input type="hidden" name="MM_insert" value="form1" />
</form>
  </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>