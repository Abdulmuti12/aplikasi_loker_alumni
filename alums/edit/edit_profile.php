
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE alumni SET nama_lengkap=%s, tgl_lahir=%s, alamat=%s, email=%s, notlpn=%s WHERE id_alumni=%s",
                       GetSQLValueString($_POST['nama_lengkap'], "text"),
                       GetSQLValueString($_POST['tgl_lahir'], "text"),
                       GetSQLValueString($_POST['alamat'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['notlpn'], "text"),
                       GetSQLValueString($_POST['id_alumni'], "text"));

  
  $Result1 = mysql_query($updateSQL) or die(mysql_error());

  if (isset($_SERVER['QUERY_STRING'])) {
echo '<META HTTP-EQUIV="Refresh" Content="0; URL=index.php?aksi=10">';



      }

 }
$colname_kone = "-1";
if (isset($_GET['id_alumni'])) {
  $colname_kone = $_GET['id_alumni'];
}

$query_kone = sprintf("SELECT id_alumni, nama_lengkap, tgl_lahir, alamat, email, notlpn FROM alumni WHERE id_alumni = %s", GetSQLValueString($colname_kone, "int"));
$kone = mysql_query($query_kone) or die(mysql_error());
$row_kone = mysql_fetch_assoc($kone);
$totalRows_kone = mysql_num_rows($kone);
?>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="bootstrap.css" rel="stylesheet" type="text/css">
<body><br>

<br>

<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1" class=" form-control-static">
  <img src="icon/profile_1x.png" height="140" width="250" class="img-circle">
  <table align="center" class="table table-hover tab-pane">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Id alumni:</td>
      <td><?php echo $row_kone['id_alumni']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nama_lengkap:</td>
      <td><input type="text" name="nama_lengkap" class="form-control" value="<?php echo htmlentities($row_kone['nama_lengkap'], ENT_COMPAT, ''); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Tanggal Lahir:</td>
      <td><input type="text" name="tgl_lahir" class="form-control" value="<?php echo htmlentities($row_kone['tgl_lahir'], ENT_COMPAT, ''); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Alamat:</td>
      <td><input type="text" name="alamat" class="form-control" value="<?php echo htmlentities($row_kone['alamat'], ENT_COMPAT, ''); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Email:</td>
      <td><input type="text" name="email" class="form-control" value="<?php echo htmlentities($row_kone['email']); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Notlpn:</td>
      <td><input type="text" name="notlpn" class="form-control" value="<?php echo htmlentities($row_kone['notlpn'], ENT_COMPAT, ''); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><button type="submit" class="btn btn-default"><i class=" fa fa-refresh ">Update</i></button>
      <button type="submit" class="btn btn-danger"><i class=" fa fa-exclamation-triangle ">Cancel</i></button></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_alumni" value="<?php echo $row_kone['id_alumni']; ?>" />
</form>
<p>&nbsp;</p>
</body>
<?php
mysql_free_result($kone);
?>
