<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halaman Admin</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<?php require_once('Connections/connection.php'); ?>
<?php include "greating.php"; ?>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="fa fa-bar"></span> <span class="fa fa-bar"></span> <span class="fa fa-bar"></span> </button>
          <a class="navbar-brand" href="index.html">Admin</a> </div>
        <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a></div>
      </nav>
      <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center"><img src="gambar/<?php echo $hasil['gambar']; ?>" width="200" height="200" class="user-image img-responsive " ></li>
				
					
                    <li>
                        <a  href="admin.php"><i class="fa fa-dashboard fa-3x"></i> Profile</a>
                    </li>
                      <li>
                        <a class="active-menu"   href="jurusa_adminpage.php"><i class="fa fa-desktop fa-3x"></i>Jurusan</a>
                    </li>
                    <li>
                        <a  href="alumni_adminpage.php"><i class="fa fa-qrcode fa-3x"></i>Alumni</a>
                    </li>
						   <li  >
                        <a  href="perusahaan_adminpage.php"><i class="fa fa-bar-chart-o fa-3x"></i> Perusahaan</a>
                    </li>	
                      <li  >
                        <a  href="table.html"><i class="fa fa-table fa-3x"></i> Table Examples</a>
                    </li>
                    <li  >
                        <a  href="komfir_admin.php"><i class="fa fa-edit fa-3x"></i> Komfirmasi </a>
                    </li>				
					
					                   
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                  </li>  
                  <li  >
                        <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
                  </li>	
                </ul>
               
            </div>
            
      </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
          <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Page Jurusan</h2>   
                        <h5>Halaman , Untuk mengelola data jurusan </h5>
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
                 <div class="row"></div>
                <!-- /. ROW  -->
                <div class="row"></div>
                 <!-- /. ROW  -->
                <div class="row"></div>
                 <!-- /. ROW  -->
                <div class="row"></div>
                <!-- /. ROW  -->
            <div class="row"></div>
              <!-- /. ROW  -->
   
   <?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO jurusan (jurusan,keterangan) VALUES (%s, %s)",
                       GetSQLValueString($_POST['jurusan'], "text"),
					   GetSQLValueString($_POST['keterangan'], "text"));

  mysql_select_db($database_connection, $connection);
  $Result1 = mysql_query($insertSQL, $connection) or die(mysql_error());

  $insertGoTo = "jurusa_adminpage.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }

}

mysql_select_db($database_connection, $connection);
$query_jurusan = "SELECT * FROM jurusan";
$jurusan = mysql_query($query_jurusan, $connection) or die(mysql_error());
$row_jurusan = mysql_fetch_assoc($jurusan);
$totalRows_jurusan = mysql_num_rows($jurusan);
?>

  <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
     
    <table align="center">
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">Jurusan:</td>
        <td><input type="text" name="jurusan" value="" size="32" /></td>
      </tr>
     <tr valign="baseline">
        <td nowrap="nowrap" align="right">Keterangan</td>
        <td><input type="text" name="keterangan" value="" size="32" /></td>
      </tr>
      
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">&nbsp;</td>
        <td><input type="submit" value="tambah" /></td>
      </tr>
    </table>
    <input type="hidden" name="MM_insert" value="form1" />
    <br />
  </form>
  <br />
  <div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th ><div align="center">Id Jurusan</div></th>
      <th><div align="center">Jurusan</div></th>
       <th><div align="center">Keterangan</div></th>
     <th colspan="2"><div align="center">Tindakan</div></th> 
    </tr>
    </thead>
    <?php do { ?>
      <tr>
        <td><div align="center"><?php echo $row_jurusan['id_jurusan']; ?></div></td>
        <td><div align="center"><?php echo $row_jurusan['jurusan']; ?></div></td>
        <td><div align="center"><?php echo $row_jurusan['keterangan']; ?></div></td>
        <td><div align="center"><a href="edit_jur.php?id_jurusan=<?php echo $row_jurusan['id_jurusan']; ?>" class="btn btn-primary btn-xs" > Edit</a> &nbsp;<a href="delete_jurusan.php?id_jurusan=<?php echo $row_jurusan['id_jurusan']; ?>" class="btn btn-danger btn-xs">Hapus</a></div></td>
      </tr>
      <?php } while ($row_jurusan = mysql_fetch_assoc($jurusan)); ?>
  </table>
</div>
   
   </div>
        </div>
         <!-- /. PAGE WRAPPER  -->
</div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
 
</body>
</html>

<?php
mysql_free_result($jurusan);
?>
