<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halaman Admin</title>
    <script src="assets/js/jquery-1.10.2.js"></script>
	<script src="assets/js/highcharts.js"></script>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<?php include "greating.php"; ?>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                </button>
                
                  <a class="navbar-brand" href="index.html">admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> 
<li class="dropdown">
                    <a class="dropdown-header" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"p class="btn btn-danger square-btn-adjust"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="admin.php"><i class="fa fa-user fa-fw"></i>Admin</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
</div>
        </nav>
                 
           <!-- /. NAV TOP  -->
      <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center"><img src="gambar/<?php echo $hasil['gambar']; ?>" width="200" height="200" class="user-image img-responsive" ></li>
				
					
                    <li>
                        <a  href="dashboard.php"><i class="fa fa-dashboard fa-3x"></i>Dashboard</a>
                    </li>
                      <li>
                        <a  href="jurusan_admi.php"><i class="fa fa-desktop fa-3x"></i>Jurusan</a>
                    </li>
                    <li>
                        <a   href="alumni_adminpage.php"><i class="fa fa-qrcode fa-3x"></i>Alumni</a>
                    </li>
						   <li  >
                        <a  href="perusahaan_adminpage.php"><i class="fa fa-bar-chart-o fa-3x"></i> Perusahaan</a>
                    </li>	
                      <li  >
                        <a  href="loker_adminpage.php"><i class="fa fa-table fa-3x"></i>Loker</a>
                    </li>
                    
					                   
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a   href="jurusan_lap.php">Jurusan</a>
                            </li>
                            <li>
                                <a  href="laporan_alum.php">Alumni</a>
                            </li>
                             <li>
                                <a  class="active-menu" href="laporan_perusahaan.php">Perusahaan</a>
                            </li>
                          
                                    

                                </ul>
                               
                            </li>
                        </ul>
                      </li>  
                  
                </ul>
               
        </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
              <div class="row"></div>
                 <!-- /. ROW  -->
              <div class="row"></div>
                   <!-- /. ROW  --><!-- /. ROW  -->
            <div class="row"></div>
                    <!-- /. ROW  -->
              <div class="row"></div>
                    <!-- /. ROW  --><!-- /. ROW  -->



	<!-- Bagian css -->
    
<?php	
include "conat.php";

$tot=mysql_query("SELECT COUNT(id_perusahaan) from perusahaan");
$holda=mysql_query("SELECT COUNT(id_perusahaan) from perusahaan where   surat='BELUM' and setuju='TUNGGU' ");
$hold1=mysql_query("SELECT COUNT(id_perusahaan) from perusahaan where surat='SUDAH' and setuju='TUNGGU' ");
$hold2=mysql_query("SELECT COUNT(*) from perusahaan where surat='SUDAH' and setuju='YA' ");
$hbljr=mysql_query("SELECT COUNT(id_perusahaan) from perusahaan where surat='SUDAH' and setuju='TIDAK'  ");
$total=mysql_result($tot,0);
$kerja=mysql_result($hold1, 0); 
$juma=mysql_result($holda, 0);
$jum2=mysql_result($hold2, 0);
$bljr=mysql_result($hbljr, 0);
 ?>

	
	
	<script>
		var chart1; 
		$(document).ready(function() {
			  chart1 = new Highcharts.Chart({
				 chart: {
					renderTo: 'mygraph',
					type: 'column'
				 },   
				 title: {
					text: 'Perusahaan Statistics '
				 },
				 xAxis: {
					categories: ['Perusahaan']
				 },
				 yAxis: {
					title: {
					   text: 'Data Perusahaan'
					}
				 },
					  series:             
					[
					 
						
							{
							  name: '<?php  echo"Terkomfimasi"; ?>',
							  data: [<?php echo $jum2; ?>]
							},
							{
							  name: '<?php  echo"Ditolak"; ?>',
							  data: [<?php echo $bljr; ?>]
							},
							{
							  name: '<?php  echo"Menunggu Persetujuan"; ?>',
							  data: [<?php echo $kerja; ?>]
							},
							{
							  name: '<?php  echo"Belum Mengajukan"; ?>',
							  data: [<?php echo $juma; ?>]
							},
							<?php 
						
						 	?>
						
						]
			  });
		   });	
	</script>

<!--- Bagian Judul-->	

	<div class="col-md-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="fa fa-bookmark"></i>Laporan Grafik Perusahaan</div>
				<div class="panel-body">
					<div id ="mygraph"></div>
				Total 
 <?php echo $total; ?> Perusahaan                </div>
		</div>
	</div>






              </div>
           </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
        
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
