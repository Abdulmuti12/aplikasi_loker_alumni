<?php require_once('../Connections/connection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "-1";
if (isset($_GET['id_perusahaan'])) {
  $colname_Recordset1 = $_GET['id_perusahaan'];
}
mysql_select_db($database_connection, $connection);
$query_Recordset1 = sprintf("SELECT * FROM perusahaan WHERE id_perusahaan = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $connection) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> Admin</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<?php include "greating.php"; ?>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> <a href="#" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="gambar/<?php echo $hasil['gambar']; ?>" width="200" height="200" class="user-image img-responsive" > 
				  </li>
				
					
                    <li>
                        <a  href="dashboard.php"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
                      <li>
                        <a  href="jurusan_admi.php"><i class="fa fa-desktop fa-3x"></i> Jurusan</a>
                    </li>
                    <li>
                        <a  href="alumni_adminpage.php"><i class="fa fa-qrcode fa-3x"></i>Alumni</a>
                    </li>
						   <li  >
                        <a  href="chart.html"><i class="fa fa-bar-chart-o fa-3x"></i> Perusahaan</a>
                    </li>	
                      <li  >
                        <a  href="table.html"><i class="fa fa-table fa-3x"></i> Loker</a>
                    </li>
                   			
					
					                   
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                  </li>  
               
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
</p>
                 
        <div id="page-wrapper" >
          <div id="page-inner">
            <div class="row">
              <div class="col-md-12">
     <div class="col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        
                       <center><h4><?php echo $row_Recordset1['nama_perusahaan']; ?></h4>
                         <?php echo $row_Recordset1['alamat_perusahaan']; ?>
                         </center>
                    
                                           </div>
                                           <div class="panel-body">
                                           <div class="col-md-6 col-sm-6">
                                           <table class="table">
                  <tr>
                                          <th>Nama Perwakilan</th><td>: <?php echo $row_Recordset1['nama_pencari']; ?></td>
                                          </tr>
                                          <tr>
                                           <th>Email Perusahaan</th> <td>: <?php echo $row_Recordset1['email_perusahaan']; ?></td>
                                           </tr>
                                           <tr>
                                          <th>Nomor Telephone</th> <td>: <?php echo $row_Recordset1['telp_perusahaan']; ?></td>
                                          </tr>
                                           </table>
                                           </div>
                                           

                                           
                                           
                                            <div class="col-md-6 col-sm-6">
                                           <table class="table">
                                         
<tr>                                           <th> Komfirmasi status</th> <td>: <?php echo $row_Recordset1['setuju']; ?></td>
</tr>                                      

<tr>
                                           <th>Pengajuan </th><td><?php echo $row_Recordset1['surat']; ?></td>
</tr>


<tr>

                                           <th> keterangan perusahaan</th>
                                           </tr>
                                                                                      <tr>
                                           <td>
                                             <textarea cols="50" role="4" class="form-control text-center"><?php echo $row_Recordset1['keterangan_perusahaan']; ?>
                                             </textarea>
                                             </td>
                                             </tr>
                                             </table>
                                             </div>
                                           
                                           
                                           
                                           </div>
                        
                        
                        
                        
                        
                        
                                                <div class="panel-footer">
                           Dikomfirmasi: <?php echo $row_Recordset1['id_admin']; ?>
                        </div>
                    </div>
                </div>                        
                     
                
              
             <!-- /. PAGE INNER  -->
      </div>
         <!-- /. PAGE WRAPPER  -->
</div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
    
   
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
