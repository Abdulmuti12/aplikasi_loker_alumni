
<?php require_once('Connections/connection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO alumni (nama_lengkap, jenis_kelamin, id_jurusan, tgl_lahir, alamat, gambar, email, notlpn, nis) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['nama_lengkap'], "text"),
                       GetSQLValueString($_POST['jenis_kelamin'], "text"),
                       GetSQLValueString($_POST['id_jurusan'], "text"),
                       GetSQLValueString($_POST['tgl_lahir'], "text"),
                       GetSQLValueString($_POST['alamat'], "text"),
                       GetSQLValueString($_POST['gambar'], "text"),
                       GetSQLValueString($_POST['email'], "text"),
                       GetSQLValueString($_POST['notlpn'], "text"),
                       GetSQLValueString($_POST['nis'], "text"));

  mysql_select_db($database_connection, $connection);
  $Result1 = mysql_query($insertSQL, $connection) or die(mysql_error());

  $insertGoTo = "alumni_page.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connection, $connection);
$query_alumni = "SELECT nama_lengkap, jenis_kelamin, id_jurusan, tgl_lahir, alamat, gambar, email, notlpn, nis FROM alumni";
$alumni = mysql_query($query_alumni, $connection) or die(mysql_error());
$row_alumni = mysql_fetch_assoc($alumni);
$totalRows_alumni = mysql_num_rows($alumni);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center"><a href="admin_page.php">Kelola admin</a>
  | <a href="jurusan_page.php">Jurusan</a> | <a href="alumni_page.php">Alumni</a></div>
</form>
<p>&nbsp;</p>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nama_lengkap:</td>
      <td><input type="text" name="nama_lengkap" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Jenis_kelamin:</td>
      <td><input type="radio" name="jenis_kelamin" id="jenis kelamin" value="laki-laki" />
      <label for="jenis kelamin">laki-laki </label>
      <input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan" />
      <label for="wanita">perempuan</label></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Id_jurusan:</td>
      <td><label for="select"></label>
        <select name="id_jurusan" id="id_jurusan">
      <?php
        $tampil=mysql_query("SELECT * FROM jurusan ORDER BY id_jurusan");
echo "</span><option value='belum milih' selected>- Masukan Penerima -</option>";
while($w=mysql_fetch_array($tampil))
{

	
	echo "<option value='$w[jurusan]' selected>$w[jurusan]</option>";        

}
?>

      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Tgl_lahir:</td>
      <td><input type="text" name="tgl_lahir" value="" size="32" /></td>
    </tr> 
   
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">NIM:</td>
      <td><input type="text" name="nis" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Insert record" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>
<p>&nbsp;</p>
<div align="center">
  <table border="1">
    <tr>
      <td>nama_lengkap</td>
      <td>jenis_kelamin</td>
      <td>id_jurusan</td>
      <td>tgl_lahir</td>
      <td>alamat</td>
      <td>gambar</td>
      <td>email</td>
      <td>notlpn</td>
      <td>nis</td>
    </tr>
    <?php do { ?>
      <tr>
        <td><?php echo $row_alumni['nama_lengkap']; ?></td>
        <td><?php echo $row_alumni['jenis_kelamin']; ?></td>
        <td><?php echo $row_alumni['id_jurusan']; ?></td>
        <td><?php echo $row_alumni['tgl_lahir']; ?></td>
        <td><?php echo $row_alumni['alamat']; ?></td>
        <td><?php echo $row_alumni['gambar']; ?></td>
        <td><?php echo $row_alumni['email']; ?></td>
        <td><?php echo $row_alumni['notlpn']; ?></td>
        <td><?php echo $row_alumni['nis']; ?></td>
      </tr>
      <?php } while ($row_alumni = mysql_fetch_assoc($alumni)); ?>
  </table>
</div>
</body>
</html>
<?php
mysql_free_result($alumni);
?>
