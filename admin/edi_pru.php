<?php require_once('../Connections/connection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE perusahaan SET id_perusahaan=%s, nama_pencari=%s, email_perusahaan=%s, telp_perusahaan=%s, keterangan_perusahaan=%s, password_perusahaan=%s, alamat_perusahaan=%s, surat=%s, setuju=%s, nama_surat=%s WHERE nama_perusahaan=%s",
                       GetSQLValueString($_POST['id_perusahaan'], "int"),
                       GetSQLValueString($_POST['nama_pencari'], "text"),
                       GetSQLValueString($_POST['email_perusahaan'], "text"),
                       GetSQLValueString($_POST['telp_perusahaan'], "text"),
                       GetSQLValueString($_POST['keterangan_perusahaan'], "text"),
                       GetSQLValueString($_POST['password_perusahaan'], "text"),
                       GetSQLValueString($_POST['alamat_perusahaan'], "text"),
                       GetSQLValueString($_POST['surat'], "text"),
                       GetSQLValueString($_POST['setuju'], "text"),
                       GetSQLValueString($_POST['nama_surat'], "text"),
                       GetSQLValueString($_POST['nama_perusahaan'], "text"));

  mysql_select_db($database_connection, $connection);
  $Result1 = mysql_query($updateSQL, $connection) or die(mysql_error());

  $updateGoTo = "perusahaan_page.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_pru = "-1";
if (isset($_GET['nama_perusahaan'])) {
  $colname_pru = $_GET['nama_perusahaan'];
}
mysql_select_db($database_connection, $connection);
$query_pru = sprintf("SELECT * FROM perusahaan WHERE nama_perusahaan = %s", GetSQLValueString($colname_pru, "text"));
$pru = mysql_query($query_pru, $connection) or die(mysql_error());
$row_pru = mysql_fetch_assoc($pru);
$totalRows_pru = mysql_num_rows($pru);

mysql_free_result($pru);
?>

<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    
<input type="hidden" name="id_perusahaan" value="<?php echo htmlentities($row_pru['id_perusahaan'], ENT_COMPAT, ''); ?>" size="32">
   
    <tr valign="baseline">
      <td nowrap align="right">Nama Perusahaan:</td>
      <td><?php echo $row_pru['nama_perusahaan']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Nama Pencari:</td>
      <td><?php echo $row_pru['nama_pencari']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Email_perusahaan:</td>
      <td><?php echo $row_pru['email_perusahaan']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Telephone:</td>
      <td><?php echo $row_pru['telp_perusahaan']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Keterangan Perusahaan:</td>
      <td>
      <textarea name="textarea" id="textarea" cols="40" rows="4" readonly><?php echo htmlentities($row_pru['keterangan_perusahaan'], ENT_COMPAT, ''); ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Alamat_perusahaan:</td>
      <td><input type="text" name="alamat_perusahaan" value="<?php echo htmlentities($row_pru['alamat_perusahaan'], ENT_COMPAT, ''); ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Surat:</td>
      <td><input type="text" name="surat" value="<?php echo htmlentities($row_pru['surat'], ENT_COMPAT, ''); ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Setuju:</td>
      <td><input type="radio" name="setuju" id="setuju2" value="YA">        
        Ya &nbsp;
      <input type="radio" name="setuju" id="setuju" value="tidak"> Tidak</td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Nama_surat:</td>
      <td><input type="text" name="nama_surat" value="<?php echo htmlentities($row_pru['nama_surat'], ENT_COMPAT, ''); ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="Update record"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="nama_perusahaan" value="<?php echo $row_pru['nama_perusahaan']; ?>">
</form>
<p>&nbsp;</p>
