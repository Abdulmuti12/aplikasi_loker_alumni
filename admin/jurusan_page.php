<?php require_once('Connections/connection.php'); ?>
<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "login1.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO jurusan (jurusan) VALUES (%s)",
                       GetSQLValueString($_POST['jurusan'], "text"));

  mysql_select_db($database_connection, $connection);
  $Result1 = mysql_query($insertSQL, $connection) or die(mysql_error());

  $insertGoTo = "jurusan_page.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_connection, $connection);
$query_jurusan = "SELECT * FROM jurusan";
$jurusan = mysql_query($query_jurusan, $connection) or die(mysql_error());
$row_jurusan = mysql_fetch_assoc($jurusan);
$totalRows_jurusan = mysql_num_rows($jurusan);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center"><a href="admin_page.php">Kelola admin</a>
  | <a href="jurusan_page.php">Jurusan</a> | <a href="alumni_page.php">Alumni</a> | <a href="<?php echo $logoutAction ?>">Logout</a></div>
</form>
<div align="center">
  <p>&nbsp;</p>
  
  <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
     <div class="form-control-static">
                    <input type="text" class="form-control text-center" name="telp_perusahaan" placeholder="Nomor Tlpn">
                </div>
    <table align="center">
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">Jurusan:</td>
        <td><input type="text" name="jurusan" value="" size="32" /></td>
      </tr>
      <tr valign="baseline">
        <td nowrap="nowrap" align="right">&nbsp;</td>
        <td><input type="submit" value="tambah" /></td>
      </tr>
    </table>
    <input type="hidden" name="MM_insert" value="form1" />
    <br />
  </form>
  <br />
  <table border="1">
    <tr>
      <td><div align="center">Id jurusan</div></td>
      <td><div align="center">Jurusan</div></td>
     <td colspan="2"><div align="center">Tindakan</div></td> 
    </tr>
    <?php do { ?>
      <tr>
        <td><div align="center"><?php echo $row_jurusan['id_jurusan']; ?></div></td>
        <td><div align="center"><?php echo $row_jurusan['jurusan']; ?></div></td>
        <td><div align="center"><a href="edit_jur.php?id_jurusan=<?php echo $row_jurusan['id_jurusan']; ?>">Edit</a></div><div align="center">Hapus</div></td>
      </tr>
      <?php } while ($row_jurusan = mysql_fetch_assoc($jurusan)); ?>
  </table>
</div>
</body>
</html>
<?php
mysql_free_result($jurusan);
?>
