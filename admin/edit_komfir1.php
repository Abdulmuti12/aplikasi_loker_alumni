<?php require_once('../Connections/connection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE perusahaan SET id_admin=%s,setuju=%s, surat=%s WHERE id_perusahaan=%s",
                        GetSQLValueString($_POST['id_admin'], "text"),
					   GetSQLValueString($_POST['setuju'], "text"),
                       GetSQLValueString($_POST['surat'], "text"),
                       GetSQLValueString($_POST['id_perusahaan'], "int"));

  mysql_select_db($database_connection, $connection);
  $Result1 = mysql_query($updateSQL, $connection) or die(mysql_error());

  $updateGoTo = "perusahaan_adminpage.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset1 = "-1";
if (isset($_GET['id_perusahaan'])) {
  $colname_Recordset1 = $_GET['id_perusahaan'];
}
mysql_select_db($database_connection, $connection);
$query_Recordset1 = sprintf("SELECT id_perusahaan,id_admin, nama_perusahaan, surat, setuju, nama_surat FROM perusahaan WHERE id_perusahaan = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $connection) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin Page</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<?php include "greating.php"; ?>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin.php">Halaman admin</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> <a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                   <img src="gambar/<?php echo $hasil['gambar']; ?>" width="200" height="200" class="user-image img-responsive" > 
					
                    </li>
				

					
                  <li>
                        <a class="active-menu"  href="admin.php"><i class="fa fa-dashboard fa-3x"></i> Profile </a>
                    </li>
                  <li>
                        <a  href="jurusa_adminpage.php"><i class="fa fa-desktop fa-3x"></i>Jurusan</a>
                    </li>
                  <li>
                        <a  href="alumni_adminpage.php"><i class="fa fa-qrcode fa-3x"></i>Alumni</a>
                    </li>
			      <li  >
                        <a   href="chart.html"><i class="fa fa-bar-chart-o fa-3x"></i>Perusahaan</a>
                    </li>	
                  <li  >
                        <a  href="loker_adminpage.php"><i class="fa fa-table fa-3x"></i> Loker</a>
                    </li>
                  <li  >
                        <a  href="komfirmasi_adminpage.php"><i class="fa fa-edit fa-3x"></i> Komfirmasi </a>
                    </li>				
					
					                   
                  <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                      </li>  
                  <li  >
                        <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
                    </li>	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
          <div id="page-inner">
            <div class="row">
              <div class="col-md-12">
                  
                                          </div>
            </div> 
             <br>
<br>
<div class="col-md-8col-sm-8">
   <div class="panel panel-default">
                        <div class="panel-heading">
                    Komfirmasi Persetujuan
                                           </div>
                         <div class="panel-body">
<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    <tr>
    <td><input type="hidden" value="<?php echo"$username"; ?>" name="id_admin">
   <tr valign="baseline">
      <td nowrap="nowrap" align="right">Setuju:</td>
      <td><label>
        <select name="setuju" id="select">
        <option value="<?php echo htmlentities($row_Recordset1['setuju'], ENT_COMPAT, 'utf-8'); ?>"><?php echo htmlentities($row_Recordset1['setuju'], ENT_COMPAT, 'utf-8'); ?></option>
        <option value="YA">YA</option>
        <option value="Tidak">Tidak</option>
        </select>
      </label></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Surat:</td>
      <td><input type="text" name="surat" readonly value="<?php echo htmlentities($row_Recordset1['surat'], ENT_COMPAT, 'utf-8'); ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td><br>

      <td></td>
    </tr>
  </table>
  <input type="submit" value="Komfirmasi" class="btn btn-default">
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="id_perusahaan" value="<?php echo $row_Recordset1['id_perusahaan']; ?>">
</form>
   </div>
                                                <div class="panel-footer"></div>
                    </div>
</div>

<p>&nbsp;</p>
<!-- /. ROW  -->
                <div class="row"></div>
                 <!-- /. ROW  -->
                <div class="row"></div>
                 <!-- /. ROW  -->
                <div class="row"></div>
                 <!-- /. ROW  -->
                <div class="row" ></div>
                 <!-- /. ROW  -->
                <div class="row"></div>     
                 <!-- /. ROW  -->           
    </div>
             <!-- /. PAGE INNER  -->
      </div>
         <!-- /. PAGE WRAPPER  -->
</div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
    
   
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
