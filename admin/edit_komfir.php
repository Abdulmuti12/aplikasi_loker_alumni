<?php require_once('../Connections/connection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE perusahaan SET setuju=%s, surat=%s WHERE id_perusahaan=%s",
                       GetSQLValueString($_POST['setuju'], "text"),
                       GetSQLValueString($_POST['surat'], "text"),
                       GetSQLValueString($_POST['id_perusahaan'], "int"));

  mysql_select_db($database_connection, $connection);
  $Result1 = mysql_query($updateSQL, $connection) or die(mysql_error());

  $updateGoTo = "komfir_admin.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_sty = "-1";
if (isset($_GET['id_perusahaan'])) {
  $colname_sty = $_GET['id_perusahaan'];
}
mysql_select_db($database_connection, $connection);
$query_sty = sprintf("SELECT id_perusahaan, surat, setuju FROM perusahaan WHERE id_perusahaan = %s", GetSQLValueString($colname_sty, "int"));
$sty = mysql_query($query_sty, $connection) or die(mysql_error());
$row_sty = mysql_fetch_assoc($sty);
$totalRows_sty = mysql_num_rows($sty);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Id_perusahaan:</td>
      <td><?php echo $row_sty['id_perusahaan']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Setuju:</td>
      <td><label>
        <select name="setuju" id="select">
        <option value="<?php echo htmlentities($row_sty['setuju'], ENT_COMPAT, 'utf-8'); ?>"><?php echo htmlentities($row_sty['setuju'], ENT_COMPAT, 'utf-8'); ?></option>
        <option value="YA">YA</option>
        <option value="Tidak">Tidak</option>
        </select>
      </label></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Surat:</td>
      <td><input type="text" name="surat" value="<?php echo htmlentities($row_sty['surat'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Update record" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_perusahaan" value="<?php echo $row_sty['id_perusahaan']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($sty);
?>
