<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> Admin</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<?php include "greating.php";?>
  <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">admin</a> 
            </div>
   <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> 
<li class="dropdown">
                    <a class="dropdown-header" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"p class="btn btn-danger square-btn-adjust"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="admin.php"><i class="fa fa-user fa-fw"></i>Admin</a>
                        </li>
                        <li><a href="proses.php"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
</div>
        </nav>   
  
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                   
				  </li>
				<li class="text-center"> <img src="gambar/<?php echo $hasil['gambar']; ?>" width="200" height="200" class="user-image img-responsive" > 
					</li>
				
					
                    <li>
                        <a   class="active-menu" href="dashboard.php"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
                    <li>
                        <a  href="jurusan_admi.php"><i class="fa fa-desktop fa-3x"></i> Jurusan</a>
                    </li>
                    <li>
                        <a  href="alumni_adminpage.php"><i class="fa fa-qrcode fa-3x"></i>Alumni</a>
                    </li>
						   <li  >
                        <a    href="perusahaan_adminpage.php"><i class="fa fa-bar-chart-o fa-3x"></i>perusahaan</a>
                    </li>	
                      <li  >
                        <a  href="loker_adminpage.php"><i class="fa fa-table fa-3x"></i>Loker</a>
                    </li>
                   			
					
					                     
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Laporan<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a   href="jurusan_lap.php">Jurusan</a>
                            </li>
                            <li>
                                <a  href="laporan_alum.php">Alumni</a>
                            </li>
                            <li>
                                <a href="#">Perusahaan<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                         <a href="laporan_perusahaan.php">Data Perusahaan</a>
                                    </li>
                                    <li>
                                        <a href="#">Loker</a>
                                    </li>
                                   

                                </ul>
                               
                            </li>
                        </ul>
                      </li>  
                  
                </ul>
               
            </div>
            
        </nav>  
  
     <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Admin Dashboard</h2>   
                      
                    </div>
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
            <a href="admin_data.php">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-user"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">Admin</p>
                                   </div>
                                   </a>
             </div>
		     </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
            <a href="jurusan_admi.php">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-desktop "></i>
                </span>
                <div class="text-box" >

                    <p class="main-text">Jurusan</p>
                   
                </div>

</a>             </div>
		     </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
            <a href="perusahaan_adminpage.php">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa fa-bar-chart-o"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">Perusahaan</p>
                    
                </div>
                </a>
             </div>
		     </div>
                  
             
              <div class="col-md-4 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
            <a href="alumni_adminpage.php">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-qrcode"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">Alumni</p>
                   
                </div>
                </a>
             </div>
		     </div>
             
               <div class="col-md-4 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
                <a href="loker_adminpage.php">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa fa-table"></i>
                </span>
                <div class="text-box" >
                    <p class="main-text">Loker</p>
                   
                </div>
                </a>
             </div>
		     </div>
             
               <div class="col-md-3 col-sm-6 col-xs-6">           
			<div class="panel panel-back noti-box">
            <a href="sjd.php">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-archive"></i>
                </span>
               
                <div class="text-box" >
                    <p class="main-text">Laporan</p>
                   
                </div>
                 </a>
             </div>
		     </div>
             
			</div>
                 <!-- /. ROW  -->
                    
                </div></div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
