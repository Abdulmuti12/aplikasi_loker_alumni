<?php require_once('Connections/connection.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_connection, $connection);
$perpage=3;
$page=isset($_GET["halaman"]) ? (int)$_GET["halaman"]: 1;
$start=($page > 1 ) ? ($page * $perpage) - $perpage :0;

$article= "SELECT * FROM loker LIMIT $start, $perpage";
$result2=mysql_query($article);


$query_Recordset1 = "SELECT posisi, gaji, jenjang_pendidikan, jurusan, fasilitas, tugas, usia, jenis_kelamin, id_perusahaan FROM loker  ";
$Recordset1 = mysql_query($query_Recordset1, $connection) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
@$pages=ceil($total/$perpage);


?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Web Lowongan Kerja Alumni</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link href="Maxim/css/bootstrap-responsive.css" rel="stylesheet">
<link href="Maxim/css/style.css" rel="stylesheet">
<link href="Maxim/color/default.css" rel="stylesheet">
<link rel="shortcut icon" href="Maxim/img/favicon.ico">
<!-- =======================================================
    Theme Name: Maxim
    Theme URL: https://bootstrapmade.com/maxim-free-onepage-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
======================================================= -->
</head>
<body>
<!-- navbar -->
<div class="navbar-wrapper">
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<!-- Responsive navbar -->
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</a>
				<h2 class="brand"><a href="index.html">BSI</a></h2>
				<!-- navigation -->
				<nav class="pull-right nav-collapse collapse">
				<ul id="menu-main" class="nav">
					<li><a title="team" href="#about">Profile</a></li>
					<li><a title="services" href="#services">Jurusan</a></li>
					<li><a title="works" href="#works">Lowongan</a></li>
					
					<li><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Masuk
  <span class="caret"></span></button></a>
  
  <ul class="dropdown-menu">
    <li><a href="alums/login.php">Alumni</a></li>
    <li><a href="alumn/pages/login.php">Perusahaan</a></li>
  </ul>

  </li>
  
				</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- Header area -->
<div id="header-wrapper" class="header-slider">
	<header class="clearfix">
	<div class="logo">
		<img src="logo_bsi1.png" height="120" width="180" alt="" />
	</div>
	<div class="container">
		<div class="row">
			<div class="span12">
				<div id="main-flexslider" class="flexslider">
					<ul class="slides">
						<li>
						<p class="home-slide-content">
							Website Lowongan <strong>Kerja</strong>
						</p>
						</li>
						<li>
						<p class="home-slide-content">
							Alumni <strong>AMIK</strong>
						</p>
						</li>
						<li>
						<p class="home-slide-content">
							 <strong>BSI</strong>Tasikmalaya
						</p>
						</li>
					</ul>
				</div>
				<!-- end slider -->
			</div>
		</div>
	</div>
	</header>
</div>
<!-- spacer section -->

<!-- end spacer section -->
<!-- section: team -->
<section id="about" class="section">
<div class="container">
	<h4>Web Lowongan Kerja <br>
Alumni AMIK BSI Tasikmalaya</h4>
	<div class="row">
		
        <div class="span6">
		<h5>Visi dan Misi</h5>
					<div class="tabbable">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#one" data-toggle="tab"><i class="icon-rocket"></i>Visi</a></li>
								<li><a href="#two" data-toggle="tab">Misi</a></li>
								
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="one">
									<p>
										<strong>Visi Web Lowongan Kerja Alumni</strong> Menjadi Fasilitas Kampus AMIK BSI Tasikmalaya yang Dapat Mempertemukan mahasiswa/alumni dan Dunia Industri 
									</p>
															</div>
								<div class="tab-pane" id="two">
									<p>
										1. Mempersiapkan sedini mungkin perencanaan karir mahasiswa

									</p>
									<p>
										2. Mewujudkan mahasiswa/alumni BSI yang cerdas dalam meraih karir gemilang

									</p>
                                    <p>
										3. Sebagai media komunikasi antara BSI dengan dunia Industri/Usaha.


									</p>
								</div>
															</div>					
					</div>		
		</div>
		
		<div class="span6">
			<h5>Kontak Informasi</h5>
					<!-- start: Accordion -->
					<div class="accordion" id="accordion2">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
								<i class="icon-minus"></i> Alamat Kampus </a>
							</div>
							<div id="collapseOne" class="accordion-body collapse in">
								<div class="accordion-inner">
									Jl. Tanuwijaya No.4, Empangsari, Tawang, Tasikmalaya, Jawa Barat 46113

								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
								<i class="icon-plus"></i>Telephone</a>
							</div>
							<div id="collapseTwo" class="accordion-body collapse">
								<div class="accordion-inner">
									 (0265) 323846

								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
								<i class="icon-plus"></i> Email </a>
							</div>
							<div id="collapseThree" class="accordion-body collapse">
								<div class="accordion-inner">
									amik.tasikmalaya@bsi.ac.id  
								</div>
							</div>
						</div>
					</div>
					<!--end: Accordion -->	
		</div>
	</div>

        
        	</div>
</div>
<!-- /.container -->
</section>
<!-- end section: team -->
<!-- section: services -->
<section id="services" class="section">
<div class="container">
	<h4>Jurusan di BSI</h4>
	<!-- Four columns -->
<?php include "jurusan.php"; ?>	

</div>
</section>
<!-- end section: services -->
<!-- section: works -->
<section id="works" class="section">
<div class="container">
	<h4>Lowongan Kerja</h4>
	<!-- portfolio filter -->
    
	<div class="row">
	  <div id="filters" class="span12"> 
      
      </div>


      <br>


<?PHP include "tampils.php"; ?>

<!-- END PORTFOLIO FILTERING -->
	</div>
	<div class="row">
		<div class="span12"></div>
	</div>
</div>
</section>






<footer>
<div class="container">
	<div class="row">
		<div class="span6 offset3">
			<ul class="social-networks">
				<li><a href="#"><i class="icon-circled icon-bgdark icon-instagram icon-2x"></i></a></li>
				<li><a href="#"><i class="icon-circled icon-bgdark icon-twitter icon-2x"></i></a></li>
				<li><a href="#"><i class="icon-circled icon-bgdark icon-dribbble icon-2x"></i></a></li>
				<li><a href="#"><i class="icon-circled icon-bgdark icon-pinterest icon-2x"></i></a></li>
			</ul>
			<p class="copyright">
				&copy; Maxim Theme. All rights reserved.
                <div class="credits">
                    <!-- 
                        All the links in the footer should remain intact. 
                        You can delete the links only if you purchased the pro version.
                        Licensing information: https://bootstrapmade.com/license/
                        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Maxim
                    -->
                    <a href="https://bootstrapmade.com/">Free Bootstrap Themes</a> by BootstrapMade.com
                </div>
			</p>
		</div>
	</div>
</div>
<!-- ./container -->
</footer>
<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
<script src="Maxim/js/jquery.js"></script>
<script src="Maxim/js/jquery.scrollTo.js"></script>
<script src="Maxim/js/jquery.nav.js"></script>
<script src="Maxim/js/jquery.localscroll-1.2.7-min.js"></script>
<script src="Maxim/js/bootstrap.js"></script>
<script src="Maxim/js/jquery.prettyPhoto.js"></script>
<script src="Maxim/js/isotope.js"></script>
<script src="Maxim/js/jquery.flexslider.js"></script>
<script src="Maxim/js/inview.js"></script>
<script src="Maxim/js/animate.js"></script>
<script src="Maxim/js/validate.js"></script>
<script src="Maxim/js/custom.js"></script>
<script src="Maxim/contactform/contactform.js"></script>

</body>
</html>

